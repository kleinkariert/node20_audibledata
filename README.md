# Node20_AudibleData

## Links

### Practical

  - [VL.Audio](https://www.nuget.org/packages/VL.Audio/0.2.3-alpha)
  - [Asio4All](http://www.asio4all.org/)

### Pulsar Data

  - [Pulsar get request](https://www.atnf.csiro.au/people/pulsar/psrcat/proc_form.php?version=1.63&F0=F0&DM=DM&startUserDefined=true&c1_val=&c2_val=&c3_val=&c4_val=&sort_attr=Date&sort_order=asc&condition=&pulsar_names=&ephemeris=short&coords_unit=raj%2Fdecj&radius=&coords_1=&coords_2=&style=Short+csv+without+errors&no_value=00&fsize=3&x_axis=&x_scale=linear&y_axis=&y_scale=linear&state=query&table_bottom.x=53&table_bottom.y=26)

### Sunep works


  - VAudio workshop with demo, NODE17:
  - [![Touch Screen Audio Demo NODE17](http://img.youtube.com/vi/v9A2ZClc7gM/0.jpg)](https://youtu.be/v9A2ZClc7gM?t=7875 "Touch Screen Audio Demo NODE17o")
  - [![Teaser using very simple graphics to turn into audio](http://img.youtube.com/vi/RaOtuKm26hg/0.jpg)](https://youtu.be/RaOtuKm26hg "Teaser using very simple graphics to turn into audio")
  - [![Sobling](http://img.youtube.com/vi/O5WUYfO00bs/0.jpg)](https://youtu.be/O5WUYfO00bs "Sobling")

### Links from Chat

  - [VTOL Solarman](https://vtol.cc/solarman)
  - [Rhythmanalysis: Space, Time and Everyday Life](https://monoskop.org/images/d/d2/Lefebvre_Henri_Rhythmanalysis_Space_Time_and_Everyday_Life.pdf)